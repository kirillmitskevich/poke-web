import React from 'react';
import Head from 'next/head';
import Header from '../../src/components/header';
import Footer from '../../src/components/footer';
import PageContentHome from '../../src/components/page-content-home';
import { AppProviders } from '../../src/context';
import Menu from '../../src/components/menu';

const Page = () => {
  return (
    <AppProviders>
      <Head>
        <title>POKE – FOOD</title>
      </Head>

      <Header />

      <Menu />

      <PageContentHome />

      <Footer />
    </AppProviders>
  );
};

export default Page;
