import React from 'react';
import Menu from '../../src/components/menu/menu';
import Footer from '../../src/components/footer/footer';
import { AppProviders } from '../../src/context';

const Page = () => {
  return (
    <AppProviders>
      <Menu />
      DELIVERY
      <Footer />
    </AppProviders>
  );
};

export default Page;
