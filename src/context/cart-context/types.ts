export type State = {
  ready: boolean;
  loading: boolean;
  items: any[];
  quantityById: { [key: string]: number };
};

export type Action =
  | { type: 'initialize' }
  | { type: 'attachItem' }
  | { type: 'detachItem' }
  | { type: 'increaseQuantity' }
  | { type: 'decreaseQuantity' };

export type Dispatch = (action: Action) => void;
