import React, { useContext, useEffect, useReducer } from 'react';
import { Dispatch, State } from './types';
import { CartHelpers } from './helpers';
import { reducer } from './reducer';

const CartStateContext = React.createContext<State | null>(null);
const CartDispatchContext = React.createContext<Dispatch | null>(null);

export const CartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, {
    ready: false,
    loading: false,
    items: [],
    quantityById: {}
  });

  useEffect(() => {
    CartHelpers.initialize(dispatch);
  }, []);

  return (
    <CartStateContext.Provider value={state}>
      <CartDispatchContext.Provider value={dispatch}>
        {children}
      </CartDispatchContext.Provider>
    </CartStateContext.Provider>
  );
};

export const useCartState = () => {
  const context = useContext(CartStateContext);

  if (!context) {
    throw new Error(`"useCartState" is using outside of its Provider`);
  }

  return context;
};

export const useCartDispatch = () => {
  const context = useContext(CartDispatchContext);

  if (!context) {
    throw new Error(`"useCartDispatch" is using outside of its Provider`);
  }

  return context;
};
