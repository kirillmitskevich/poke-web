import { Action, State } from './types';

export const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case 'initialize': {
      return { ...state, ready: true };
    }

    default: {
      return state;
    }
  }
};
