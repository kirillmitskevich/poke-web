import { Dispatch, State } from './types';

const initialize = (dispatch: Dispatch) => dispatch({ type: 'initialize' });

const attachItem = (dispatch: Dispatch, id: string) =>
  dispatch({ type: 'attachItem' });

const detachItem = (dispatch: Dispatch, id: string) =>
  dispatch({ type: 'detachItem' });

const increaseQuantity = (dispatch: Dispatch, id: string) => {
  dispatch({ type: 'increaseQuantity' });
};

const decreaseQuantity = (dispatch: Dispatch, id: string) => {
  dispatch({ type: 'decreaseQuantity' });
};

const getItem = (state: State, id: string) => {
  const { items, quantityById } = state;

  const item = items.find(item => item.id === id);
  const quantity = quantityById[item.id];

  if (!item || !quantity) {
    return null;
  }

  return { ...item, quantity };
};

const getTotalAmount = (state: State) => {
  const { items, quantityById } = state;

  return items.reduce((acc, item) => {
    const quantity = quantityById[item.id];

    if (!quantity) {
      return acc;
    }

    return acc + quantity;
  }, 0);
};

const getTotalPrice = (state: State) => {
  const { items, quantityById } = state;

  return items.reduce((acc, item) => {
    const quantity = quantityById[item.id];

    if (!quantity) {
      return acc;
    }

    return acc + quantity * item.price;
  }, 0);
};

export const CartHelpers = {
  initialize,
  attachItem,
  detachItem,
  increaseQuantity,
  decreaseQuantity,
  getItem,
  getTotalAmount,
  getTotalPrice
};
