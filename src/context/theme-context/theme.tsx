export const theme = {
  palette: {
    common: {
      black: '#000',
      white: '#fff'
    },
    primary: {
      main: '#00b19e',
      dark: '#009686'
    },
    text: {
      primary: 'rgba(0, 0, 0, 0.87)',
      secondary: 'rgba(0, 0, 0, 0.54)',
      disabled: 'rgba(0, 0, 0, 0.38)',
      hint: 'rgba(0, 0, 0, 0.38)'
    },
    divider: 'rgba(0, 0, 0, 0.12)'
  }
};
