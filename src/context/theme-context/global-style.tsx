import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  html, body {
    height: 100%;
  }
  
  #__next {
    min-height: 100%;
    display: flex;
    flex-direction: column;
  }
  
  body {
    margin: 0;
    font-family: 'Gotham Pro', Arial, sans-serif;   
  }
  
  body.locked {
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    position: fixed;
    overflow: hidden;
  }

  * {
    box-sizing: border-box;
    -webkit-tap-highlight-color: transparent;
  }
`;
