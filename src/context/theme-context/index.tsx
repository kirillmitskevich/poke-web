import React from 'react';
import { ThemeProvider } from 'styled-components';
import { theme } from './theme';
import { GlobalStyle } from './global-style';

const ThemingProvider = ({ children }) => {
  return (
    <ThemeProvider theme={theme}>
      <React.Fragment>
        <GlobalStyle />

        {children}
      </React.Fragment>
    </ThemeProvider>
  );
};

export default ThemingProvider;
