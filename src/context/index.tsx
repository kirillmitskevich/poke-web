import React from 'react';
import { CartProvider } from './cart-context';
import ThemingProvider from './theme-context';

export const AppProviders = ({ children }) => {
  return (
    <ThemingProvider>
      <CartProvider>{children}</CartProvider>
    </ThemingProvider>
  );
};
