type Breakpoint = 'small' | 'medium' | 'large' | 'xlarge';

const breakpoints = {
  small: 576,
  medium: 768,
  large: 1024,
  xlarge: 1200
};

export function respondTo(breakpoint: Breakpoint) {
  const value = breakpoints[breakpoint] + 'px';
  return `(min-width: ${value})`;
}
