import React, { useEffect, useRef } from 'react';

export const useLockBodyScroll = locked => {
  const scrollTop = useRef(0);

  useEffect(() => {
    if (locked) {
      scrollTop.current = document.documentElement.scrollTop;
      document.body.classList.add('locked');
    } else {
      document.body.classList.remove('locked');
      document.documentElement.scrollTop = scrollTop.current;
    }
  }, [locked]);

  useEffect(() => () => document.body.classList.remove('locked'), []);
};
