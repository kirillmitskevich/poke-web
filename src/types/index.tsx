export enum MENU_ITEM_TYPE {
  POKE,
  DRINK
}

export enum SELECTION_MODE {
  SINGLE,
  MULTI
}

export type IMenuItemOption = {
  id: string;
  name: string;
  price: number;
};

export type IMenuItemExtra = {
  id: string;
  name: string;
  selectionMode: SELECTION_MODE;
  minNumOptions: number;
  maxNumOptions: number;
  options: string[];
};

export type IMenuItem = {
  id: string;
  type: MENU_ITEM_TYPE;
  name: string;
  price: number;
  description: string;
  imageUrl: string;
  extras: string[];
};
