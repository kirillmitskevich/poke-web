import React from 'react';
import styled from 'styled-components';
import Hero from './hero';
import Feed from './feed';
import Menu from './menu';

const PageContentHome = () => {
  return (
    <Root>
      <Hero />
      <Menu />
      <Feed />
    </Root>
  );
};

export default PageContentHome;

const Root = styled.div`
  flex: 1;
`;
