import React from 'react';
import styled from 'styled-components';
import Container from '../../container';
import { respondTo } from '../../../util/breakpoints';

const Hero = () => {
  return (
    <Root>
      <img src="https://s3.vitamojo.com/static/honipoke/Home-Page.jpg" alt="" />
    </Root>
  );
};

export default Hero;

const Root = styled(Container)`
  display: none;

  @media ${respondTo('medium')} {
    display: block;
  }

  img {
    width: 100%;
    border-radius: 8px;
    overflow: hidden;
  }
`;
