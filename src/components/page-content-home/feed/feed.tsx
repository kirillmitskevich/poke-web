import React from 'react';
import styled from 'styled-components';
import Container from '../../container';
import { respondTo } from '../../../util/breakpoints';

const instagramFeed = [
  'https://images.squarespace-cdn.com/content/v1/5877b0633a04110424147fcf/1527596194848-IV7RSTIHYDV92UDIACUQ/ke17ZwdGBToddI8pDm48kHP-oC_dIqxTeMbIxb0yZal7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0luj0xCD0oh5KMc0gpox0u8B7NNfE9JYpc_YofoPLxkUj-KkiXOsWRtBZOwczDBVCA/28+find+me+under+the+palm+trees.jpg?format=1000w',
  'https://images.squarespace-cdn.com/content/v1/586c10722994caa37cd851f9/1548450614166-02P54Q2GYHPEPXEY3BX6/ke17ZwdGBToddI8pDm48kHem505q6McQd8XRhQc9zkRZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpxpEURhgSBEh9ILe0HKIG3CcOCklsxWwam3CdWF6Xw7jwVv5P97BEtB5XaVnmvMtTA/image-asset.jpeg',
  'https://images.squarespace-cdn.com/content/v1/5877b0633a04110424147fcf/1527595982168-9GBC51H6WR8XZN5NVN4N/ke17ZwdGBToddI8pDm48kNNaKRi1utzXuYGna-bncC57gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0jG2lbcDYBOeMi4OFSYem8CrLeuJjM_g_u4Dz7B_5hLdc9rV2SBDYbKqEbr2oemBjg/_MG_8230.jpg?format=1000w',
  'https://images.squarespace-cdn.com/content/v1/586c10722994caa37cd851f9/1548963829972-0KNHUIMEONIH6SJ1IZCZ/ke17ZwdGBToddI8pDm48kEpVg-ILAPna1wRh-xAJ9fRZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpwEv36x-EUL2-BSQ5feDhwGCbXuJBFqZ-erYzVouT8yOb9TwqchglLQOCYTRn7ZGxI/image-asset.jpeg'
];

const Feed = () => {
  return (
    <Root>
      <Container>
        <Title>
          Следите за нам в <a href="#">Instagram</a>
        </Title>

        <Group>
          {instagramFeed.map(url => (
            <Item key={url}>
              <FeedItem href="#">
                <img src={url} alt="" />
              </FeedItem>
            </Item>
          ))}
        </Group>
      </Container>
    </Root>
  );
};

export default Feed;

const Root = styled.div`
  padding: 60px 0;
  //background-color: #000;
`;

const Title = styled.div`
  margin-bottom: 30px;
  line-height: 1.5;
  font-size: 24px;
  text-align: center;
  font-weight: 500;

  a {
    color: #00b19e;
    text-decoration: none;
    position: relative;

    &::after {
      content: '';
      width: 0;
      height: 2px;
      bottom: -5px;
      left: 0;
      position: absolute;
      display: block;
      background-color: #00b19e;
      transition: all 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.075);
    }

    &:hover::after {
      width: 100%;
    }
  }
`;

const Group = styled.div`
  margin: -10px;
  display: flex;
  flex-wrap: wrap;
`;

const Item = styled.div`
  padding: 10px;
  flex: 1 0 50%;

  @media ${respondTo('medium')} {
    flex: 1 0 25%;
  }
`;

const FeedItem = styled.a`
  position: relative;
  display: flex;

  &::before {
    content: '';
    padding-top: 100%;
    width: 100%;
    display: block;
  }

  img {
    max-width: 100%;
    max-height: 100%;
    width: 100%;
    height: 100%;
    top: 50%;
    left: 50%;
    position: absolute;
    transform: translate(-50%, -50%);
  }
`;
