import React from 'react';
import { IMenuItem } from '../../../../types';
import styled from 'styled-components';
import { formatPrice } from '../../../../util/price';
import ToCartButton from '../to-cart-button/to-cart-button';

type Props = {
  item: IMenuItem;
};

const MenuItemSimple: React.FC<Props> = props => {
  const { item } = props;

  return (
    <Root>
      <Title>{item.name}</Title>
      <Image src={item.imageUrl} alt={item.description} />
      <Price>{formatPrice(item.price)}</Price>
      <ToCartButton title="В корзину" />
    </Root>
  );
};

export default MenuItemSimple;

const Root = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Title = styled.div`
  margin-bottom: 25px;
  line-height: 1.3;
  font-size: 18px;
  font-weight: 500;
  color: ${props => props.theme.palette.text.primary};
  text-align: center;
`;

const Image = styled.img`
  margin: auto 0 25px;
  width: 100%;
`;

const Price = styled.div`
  margin-bottom: 15px;
  font-size: 22px;
  color: ${props => props.theme.palette.text.primary};
`;
