import React from 'react';
import { IMenuItem } from '../../../../types';
import styled from 'styled-components';
import { formatPrice } from '../../../../util/price';
import ToCartButton from '../to-cart-button/to-cart-button';

type Props = {
  item: IMenuItem;
};

const MenuItemComplex: React.FC<Props> = props => {
  const { item } = props;

  return (
    <Root>
      <Image src={item.imageUrl} alt={item.description} />
      <Title>{item.name}</Title>
      <Description>{item.description}</Description>

      <Footer>
        <Price>от {formatPrice(item.price)}</Price>
        <ToCartButton title="Выбрать" />
      </Footer>
    </Root>
  );
};

export default MenuItemComplex;

const Root = styled.div`
  display: flex;
  flex-direction: column;
`;

const Image = styled.img`
  margin-bottom: 20px;
  width: 100%;
  overflow: hidden;
  border-radius: 4px;
`;

const Title = styled.div`
  margin-bottom: 10px;
  line-height: 1.3;
  font-size: 20px;
  color: ${props => props.theme.palette.text.primary};
`;

const Description = styled.div`
  margin-bottom: 15px;
  font-size: 14px;
  color: ${props => props.theme.palette.text.secondary};
  line-height: 1.375;
`;

const Footer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const Price = styled.div`
  padding-right: 5px;
  font-size: 22px;
`;
