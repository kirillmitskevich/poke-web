import { MENU_ITEM_TYPE, IMenuItem } from '../../../types';

export const useMenuItems = () => {
  return items;
};

const items: IMenuItem[] = [
  {
    id: 'test-1',
    type: MENU_ITEM_TYPE.POKE,
    name: 'Sweet Ginger Chicken',
    price: 13.5,
    description:
      'Free-range chicken breast, green and sweet onion, edamame, cilantro, sesame seeds, Spicy Ginger sauce',
    imageUrl:
      'https://res.cloudinary.com/doordash/image/fetch/q_auto:eco,f_auto,w_600,c_fill/https://doordash-static.s3.amazonaws.com/media/photos/043ed2a1-c988-4eec-9a1d-09d56bf566d3-retina-large.jpg',
    extras: []
  },
  {
    id: 'test-2',
    type: MENU_ITEM_TYPE.POKE,
    name: 'Wasabi Shrimp & Scallops',
    price: 13.5,
    description:
      'Shrimp, scallops, green and sweet onion, cucumber, masago, sesame seeds, Wasabi Aioli',
    imageUrl:
      'https://res.cloudinary.com/doordash/image/fetch/q_auto:eco,f_auto,w_600,c_fill/https://doordash-static.s3.amazonaws.com/media/photos/4bb00495-914a-47cc-ae0d-cd86a6fc5ac1-retina-large.jpg',
    extras: []
  },
  {
    id: 'test-3',
    type: MENU_ITEM_TYPE.DRINK,
    name: 'Pepsi, 1 л',
    price: 2.9,
    description: '',
    imageUrl:
      'https://dodopizza-a.akamaihd.net/static/Img/Products/Drinks/ru-RU/5294b73c-6fde-4e46-be8e-bf94d26a8383.jpg',
    extras: []
  },
  {
    id: 'test-4',
    type: MENU_ITEM_TYPE.DRINK,
    name: '7Up, 1 л',
    price: 2.9,
    description: '',
    imageUrl:
      'https://dodopizza-a.akamaihd.net/static/Img/Products/Drinks/ru-RU/e609ba67-62c3-4351-b3be-34f4ead7d413.jpg',
    extras: []
  },
  {
    id: 'test-5',
    type: MENU_ITEM_TYPE.DRINK,
    name: 'Mirinda, 1 л',
    price: 2.9,
    description: '',
    imageUrl:
      'https://dodopizza-a.akamaihd.net/static/Img/Products/Drinks/ru-RU/240ed091-2746-444c-ad27-0da46dd1aa23.jpg',
    extras: []
  },
  {
    id: 'test-6',
    type: MENU_ITEM_TYPE.DRINK,
    name: 'Сок Я Апельсин, 0,97 л',
    price: 3.7,
    description: '',
    imageUrl:
      'https://dodopizza-a.akamaihd.net/static/Img/Products/Drinks/ru-RU/e0ca0cad-86c8-4066-99ef-e0ed28d041b4.jpg',
    extras: []
  },
  {
    id: 'test-7',
    type: MENU_ITEM_TYPE.DRINK,
    name: 'Сок Я Вишневый, 0,97 л',
    price: 3.7,
    description: '',
    imageUrl:
      'https://dodopizza-a.akamaihd.net/static/Img/Products/Drinks/ru-RU/7136d4b1-73ed-47f6-8c55-8eb670c84fac.jpg',
    extras: []
  },
  {
    id: 'test-8',
    type: MENU_ITEM_TYPE.DRINK,
    name: 'Сок Я Яблоко, 0,97 л',
    price: 3.7,
    description: '',
    imageUrl:
      'https://dodopizza-a.akamaihd.net/static/Img/Products/Drinks/ru-RU/2147abf0-2db1-4e0c-84a7-cd8ed602db41.jpg',
    extras: []
  }
];
