import React, { useState } from 'react';
import styled from 'styled-components';
import AmountControl from '../../../amount-control/amount-control';

type Props = {
  title: string;
};

const ToCartButton: React.FC<Props> = props => {
  const { title } = props;

  const [amount, setAmount] = useState(0);

  return (
    <Root>
      {amount === 0 ? (
        <Button onClick={() => setAmount(1)}>{title}</Button>
      ) : (
        <AmountControl amount={amount} onChange={setAmount} />
      )}
    </Root>
  );
};

export default ToCartButton;

const Root = styled.div`
  height: 40px;
  display: flex;
`;

const Button = styled.div`
  padding: 0 12px;
  display: flex;
  align-items: center;
  border-radius: 10px;
  color: ${props => props.theme.palette.primary.main};
  border: 1px solid ${props => props.theme.palette.primary.main};
  transition: all 200ms ease-in-out;
  cursor: pointer;

  &:hover {
    color: ${props => props.theme.palette.primary.dark};
    border: 1px solid ${props => props.theme.palette.primary.dark};
  }

  &:active {
    transform: scale(0.9);
  }
`;
