import React from 'react';
import styled from 'styled-components';
import MenuSection from './menu-section';
import { MENU_ITEM_TYPE, IMenuItem } from '../../../types';
import { useMenuItems } from './use-menu-items';
import MenuItemSimple from './menu-item-simple';
import MenuItemComplex from './menu-item-complex';

const Menu = () => {
  const menuItems = useMenuItems();

  const renderMenuItem = (item: IMenuItem) => {
    if (item.type === MENU_ITEM_TYPE.DRINK) {
      return <MenuItemSimple item={item} />;
    }

    return <MenuItemComplex item={item} />;
  };

  return (
    <Root>
      <MenuSection
        title=""
        maxItemsInRow={4}
        items={menuItems.filter(item => item.type === MENU_ITEM_TYPE.POKE)}
        renderItem={renderMenuItem}
      />

      <MenuSection
        title="Напитки"
        maxItemsInRow={5}
        items={menuItems.filter(item => item.type === MENU_ITEM_TYPE.DRINK)}
        renderItem={renderMenuItem}
      />
    </Root>
  );
};

export default Menu;

const Root = styled.div`
  display: flex;
  flex-direction: column;
`;
