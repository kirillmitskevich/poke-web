import React from 'react';
import styled, { css } from 'styled-components';
import Container from '../../../container';
import { IMenuItem } from '../../../../types';
import { respondTo } from '../../../../util/breakpoints';

type Props = {
  title: string;
  maxItemsInRow: number;
  items: IMenuItem[];
  renderItem: (item: IMenuItem) => JSX.Element;
};

const MenuSection: React.FC<Props> = props => {
  const { title, maxItemsInRow, items, renderItem } = props;

  return (
    <Root>
      {title && <Title>{title}</Title>}

      <List>
        {items.map(item => {
          return (
            <Item key={item.id} maxItemsInRow={maxItemsInRow}>
              {renderItem(item)}
            </Item>
          );
        })}
      </List>
    </Root>
  );
};

export default MenuSection;

const Root = styled(Container)``;

const Title = styled.div`
  padding-top: 45px;
  font-size: 36px;
  font-weight: 500;
  color: ${props => props.theme.palette.text.primary};

  @media ${respondTo('medium')} {
    padding-top: 75px;
  }
`;

const List = styled.div`
  margin: -15px -10px;
  padding-top: 30px;
  display: flex;
  flex-wrap: wrap;

  @media ${respondTo('medium')} {
    margin: -30px -10px;
    padding-top: 60px;
  }
`;

const Item = styled.div<{ maxItemsInRow: number }>`
  padding: 15px 10px;

  @media ${respondTo('medium')} {
    padding: 30px 10px;
  }

  ${props =>
    props.maxItemsInRow === 4 &&
    css`
      flex: 0 0 100%;

      @media ${respondTo('medium')} {
        flex: 0 0 50%;
      }

      @media ${respondTo('large')} {
        flex: 0 0 25%;
      }
    `};

  ${props =>
    props.maxItemsInRow === 5 &&
    css`
      flex: 0 0 50%;

      @media ${respondTo('medium')} {
        flex: 0 0 33.333%;
      }

      @media ${respondTo('large')} {
        flex: 0 0 20%;
      }
    `};
`;
