import React from 'react';
import Rail from '../rail/rail';
import styled from 'styled-components';
import Navigation from './navigation';
import PhoneCall from './phone-call';

type Props = {
  open: boolean;
  onClose: () => void;
};

const MenuRail: React.FC<Props> = props => {
  const { open, onClose } = props;

  return (
    <Root rootClassName="menu-rail" open={open} onClose={onClose}>
      <Content>
        <Navigation />

        <Divider />

        <PhoneCall />
      </Content>
    </Root>
  );
};

const Root = styled(Rail).attrs({
  rootClassName: 'menu-rail'
})`
  &.menu-rail {
    max-width: 290px;
    overflow: auto;
  }
`;

const Content = styled.div`
  padding: 30px 0 100px;
`;

const Divider = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;
  width: 100%;
  height: 1px;
  background-color: ${props => props.theme.palette.divider};
`;

export default MenuRail;
