import React from 'react';
import styled from 'styled-components';
import { RouterLink } from '../../../util/routing';

const Navigation = () => {
  return (
    <List>
      {[
        { title: 'Акции', href: '/events' },
        { title: 'Контакты', href: '/contacts' },
        { title: 'Доставка', href: '/delivery' }
      ].map(item => {
        return (
          <RouterLink href={item.href} key={item.href} passHref>
            <Item>{item.title}</Item>
          </RouterLink>
        );
      })}
    </List>
  );
};

export default Navigation;

const List = styled.div`
  display: flex;
  flex-direction: column;
`;

const Item = styled.a`
  padding: 15px 35px;
  transition: background-color 0.15s ease-in-out 0s;
  color: ${props => props.theme.palette.text.primary};
  font-size: 18px;
  font-weight: 500;
  text-decoration: none;

  &:hover {
    background-color: rgba(247, 247, 247, 1);
  }
`;
