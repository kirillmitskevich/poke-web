import React from 'react';
import styled from 'styled-components';

const PhoneCall = () => {
  return (
    <List>
      <Title>звонок по телефону:</Title>

      {[
        { title: '7788', href: 'tel:7788' },
        { title: '+375 29 192 22 48', href: 'tel:375291922248' }
      ].map(item => {
        return (
          <Item key={item.href} href={item.href}>
            {item.title}
          </Item>
        );
      })}
    </List>
  );
};

export default PhoneCall;

const List = styled.div`
  padding: 10px 35px;
  display: flex;
  flex-direction: column;
`;

const Title = styled.div`
  margin-bottom: 15px;
  font-size: 13px;
  color: ${props => props.theme.palette.text.secondary};
`;

const Item = styled.a`
  display: flex;
  color: ${props => props.theme.palette.text.primary};
  font-weight: 500;
  text-decoration: none;

  &:not(:last-child) {
    margin-bottom: 15px;
  }
`;
