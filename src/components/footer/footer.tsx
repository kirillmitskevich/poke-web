import React from 'react';
import styled from 'styled-components';
import OrganizationDetails from './organization-details';

const Footer = () => {
  return (
    <Root>
      <OrganizationDetails />
    </Root>
  );
};

export default Footer;

const Root = styled.div`
  background-color: #373535;
`;
