import React from 'react';
import styled from 'styled-components';
import Container from '../../container';

const OrganizationDetails = () => {
  return (
    <Root>
      ©2019 POKE-FOOD.by™ ИП "Воробьев К.Д", УНП 6532142 <br />
      220073 г.Минск, ул.Филимонова 18, офис 2 <br />
      Дата регистрации в Торговом реестре РБ 15.09.2019 <br />
    </Root>
  );
};

export default OrganizationDetails;

const Root = styled(Container)`
  padding: 20px 0;
  font-size: 10px;
  line-height: 14px;
  color: hsla(0, 0%, 100%, 0.4);
`;
