import React from 'react';
import { RouterLink } from '../../util/routing';
import styled from 'styled-components';

type Props = {
  className?: string;
};

const Logo: React.FC<Props> = props => {
  const { className = '' } = props;

  return (
    <RouterLink href="/" passHref>
      <Root className={className}>POKE – FOOD</Root>
    </RouterLink>
  );
};

export default Logo;

const Root = styled.a`
  display: flex;
  align-items: center;
  text-decoration: none;
  color: ${props => props.theme.palette.primary.main};
  font-weight: 500;
  font-size: 18px;
`;
