import React from 'react';
import styled from 'styled-components';

type Props = {
  className?: string;
};

const CartRailButton: React.FC<Props> = props => {
  const { className = '' } = props;

  return <Root className={className}>Корзина</Root>;
};

export default CartRailButton;

const Root = styled.div`
  padding: 0 12px;
  height: 40px;
  line-height: 40px;
  cursor: pointer;
  transition: all 200ms ease-in-out;
  color: #fff;
  border-radius: 8px;
  border: 1px solid ${props => props.theme.palette.primary.main};
  background: ${props => props.theme.palette.primary.main};

  &:hover {
    background: ${props => props.theme.palette.primary.dark};
  }

  &:active {
    transform: scale(0.9);
  }
`;
