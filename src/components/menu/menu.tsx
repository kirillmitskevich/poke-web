import React from 'react';
import styled from 'styled-components';
import BaseMenuRailButton from './menu-rail-button';
import BaseNavigation from './navigation';
import Container from '../container';
import BaseCartRailButton from './cart-rail-button';
import { respondTo } from '../../util/breakpoints';
import LogoBase from '../logo';

const Menu = () => {
  return (
    <Root>
      <Inner>
        <Logo />

        <Navigation />

        <CartRailButton />

        <MenuRailButton />
      </Inner>
    </Root>
  );
};

export default Menu;

const Root = styled.div`
  height: 60px;
  top: 0;
  position: sticky;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: #fff;
  border-bottom: 1px solid ${props => props.theme.palette.divider};
  z-index: 1;

  @media ${respondTo('medium')} {
    margin-bottom: 20px;
    height: 80px;
  }
`;

const Inner = styled(Container)`
  display: flex;
`;

const Logo = styled(LogoBase)`
  @media ${respondTo('medium')} {
    display: none;
  }
`;

const CartRailButton = styled(BaseCartRailButton)`
  display: none;

  @media ${respondTo('medium')} {
    margin-left: auto;
    display: flex;
  }
`;

const MenuRailButton = styled(BaseMenuRailButton)`
  margin-left: auto;
  display: flex;

  @media ${respondTo('medium')} {
    display: none;
  }
`;

const Navigation = styled(BaseNavigation)`
  display: none;

  @media ${respondTo('medium')} {
    display: flex;
  }
`;
