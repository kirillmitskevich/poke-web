import React, { useState } from 'react';
import styled from 'styled-components';
import MenuRail from '../../menu-rail';
import { respondTo } from '../../../util/breakpoints';

type Props = {
  className?: string;
};

const MenuRailButton: React.FC<Props> = props => {
  const { className = '' } = props;

  const [menuOpen, setMenuOpen] = useState(false);

  const handleClick = () => {
    setMenuOpen(true);
  };

  const handleClose = () => {
    setMenuOpen(false);
  };

  return (
    <React.Fragment>
      <Root className={className} onClick={handleClick}>
        <span />
        <span />
        <span />
      </Root>

      <MenuRail open={menuOpen} onClose={handleClose} />
    </React.Fragment>
  );
};

export default MenuRailButton;

const Root = styled.div`
  padding: 10px;
  width: 26px;
  height: 18px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  cursor: pointer;
  box-sizing: content-box;
  user-select: none;

  span {
    width: 100%;
    height: 2px;
    border-radius: 4px;
    background-color: rgba(0, 0, 0, 1);
  }

  @media ${respondTo('medium')} {
    display: none;
  }
`;
