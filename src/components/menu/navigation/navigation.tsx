import React from 'react';
import { useRouter } from 'next/router';
import styled, { css } from 'styled-components';
import { RouterLink } from '../../../util/routing';

type Props = {
  className?: string;
};

const Navigation: React.FC<Props> = props => {
  const { className = '' } = props;

  const router = useRouter();

  const isLinkMatched = href => href === router.pathname;

  return (
    <Root className={className}>
      {[
        { title: 'Поке', href: '/#poke' },
        { title: 'Напитки', href: '/#drinks' },
        { title: 'Акции', href: '/bonuses' },
        { title: 'Контакты', href: '/contacts' },
        { title: 'Доставка', href: '/delivery' }
      ].map(item => {
        const matched = isLinkMatched(item.href);

        return (
          <RouterLink href={item.href} key={item.href} passHref>
            <Item matched={matched}>{item.title}</Item>
          </RouterLink>
        );
      })}
    </Root>
  );
};

export default Navigation;

const Root = styled.div`
  display: flex;
  align-items: center;
`;

const Item = styled.a<{ matched: boolean }>`
  padding: 10px 0;
  text-decoration: none;
  color: ${props => props.theme.palette.text.primary};

  &:not(:last-child) {
    padding-right: 30px;
  }

  &:hover {
    color: #00b19e;
    transition: color 0.25s ease, padding 0.25s ease;
  }

  ${props =>
    props.matched &&
    css`
      color: #00b19e;
    `}
`;
