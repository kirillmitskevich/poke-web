import React from 'react';
import { createPortal } from 'react-dom';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { useLockBodyScroll } from '../../hooks/use-lock-body-scroll';
import styled from 'styled-components';
import classNames from 'classnames';

type Props = {
  open: boolean;
  className?: string;
  rootClassName?: string;
  backdropClassName?: string;
  onClose: () => void;
};

const Rail: React.FC<Props> = props => {
  const {
    children,
    open,
    onClose,
    className = '',
    rootClassName = '',
    backdropClassName = ''
  } = props;

  useLockBodyScroll(open);

  if (typeof window !== 'object') {
    return null;
  }

  const renderContent = () => {
    return [
      <CSSTransition key="backdrop" timeout={500} classNames="fade">
        <Backdrop
          className={classNames({
            [className]: !!className,
            [backdropClassName]: !!backdropClassName
          })}
          onClick={onClose}
        />
      </CSSTransition>,

      <CSSTransition key="rail" timeout={500} classNames="slide">
        <Root
          className={classNames({
            [className]: !!className,
            [rootClassName]: !!rootClassName
          })}
        >
          {children}
        </Root>
      </CSSTransition>
    ];
  };

  return createPortal(
    <TransitionGroup component={null}>
      {open && renderContent()}
    </TransitionGroup>,
    document.body
  );
};

export default Rail;

const Root = styled.div`
  width: 100vw;
  height: 100vh;
  top: 0;
  left: 0;
  position: fixed;
  background-color: #fff;
  z-index: 5;
  will-change: transform;
  transform: translateZ(0);
  transition: 500ms cubic-bezier(0.23, 1, 0.32, 1);

  &.slide-enter {
    transform: translate3d(-100%, 0, 0);
  }

  &.slide-enter-active {
    transform: translate3d(0, 0, 0);
  }

  &.slide-exit {
    transform: translate3d(0, 0, 0);
  }

  &.slide-exit-active {
    transform: translate3d(-100%, 0, 0);
  }
`;

const Backdrop = styled.div`
  width: 100%;
  height: 100%;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  position: fixed;
  z-index: 5;
  background-image: url('https://cdn.dribbble.com/users/247458/screenshots/5083720/wallpaper2.jpg');
  //background-color: rgba(0, 0, 0, 0.27);
  //transition: 500ms linear;

  &.fade-enter {
    opacity: 0;
  }

  &.fade-enter-active {
    opacity: 1;
  }

  &.fade-exit {
  }

  &.fade-exit-active {
    opacity: 0;
  }
`;
