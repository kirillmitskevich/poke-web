import React from 'react';

const IconIncrease = () => {
  return (
    <svg width="10px" height="10px" viewBox="0 0 10 10" version="1.1">
      <g>
        <rect fill="#00b19e" x="4" y="0" width="2" height="10" ry="1"></rect>
        <rect fill="#00b19e" x="0" y="4" width="10" height="2" rx="1"></rect>
      </g>
    </svg>
  );
};

export default IconIncrease;
