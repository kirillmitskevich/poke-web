import React from 'react';
import styled from 'styled-components';
import IconIncrease from './icon-increase';
import IconDecrease from './icon-decrease';

type Props = {
  amount: number;
  onChange: (amount: number) => void;
};

const AmountControl: React.FC<Props> = props => {
  const { amount, onChange } = props;

  const handleIncrease = () => {
    onChange(amount + 1);
  };

  const handleDecrease = () => {
    onChange(amount - 1);
  };

  return (
    <Root>
      <Control onClick={handleDecrease}>
        <IconDecrease />
      </Control>

      <Content>{amount}</Content>

      <Control onClick={handleIncrease}>
        <IconIncrease />
      </Control>
    </Root>
  );
};

export default AmountControl;

const Root = styled.div`
  display: flex;
`;

const Control = styled.div`
  padding: 8px 12px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  position: relative;
  border: 1px solid ${props => props.theme.palette.primary.main};
  border-radius: 10px;
  transition: all 200ms ease-in-out;
  user-select: none;

  &:active {
    transform: scale(0.9);
  }
`;

const Content = styled.div`
  padding: 0 12px;
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 18px;

  img {
    width: 10px;
    height: 10px;
  }
`;
