import styled from 'styled-components';

const Container = styled.div`
  margin: 0 auto;
  padding: 0 15px;
  max-width: 1280px;
  width: 100%;
`;

export default Container;
