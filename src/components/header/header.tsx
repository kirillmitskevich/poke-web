import React from 'react';
import styled from 'styled-components';
import Container from '../container';
import Logo from '../logo';
import PhoneCall from './phone-call';
import { respondTo } from '../../util/breakpoints';

const Header = () => {
  return (
    <Root>
      <Logo />

      <PhoneCall />
    </Root>
  );
};

export default Header;

const Root = styled(Container)`
  display: none;

  @media ${respondTo('medium')} {
    height: 60px;
    display: flex;
    align-items: center;
  }
`;
