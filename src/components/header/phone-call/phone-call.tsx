import React from 'react';
import styled from 'styled-components';

const PhoneCall = () => {
  return <Root href="tel:375291922248">+375 29 192 22 48</Root>;
};

export default PhoneCall;

const Root = styled.a`
  margin-left: 50px;
  text-decoration: none;
  transition: all 200ms ease-in-out;
  color: ${props => props.theme.palette.text.secondary};

  &:hover {
    color: ${props => props.theme.palette.text.primary};
  }
`;
